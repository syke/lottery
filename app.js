function App() {
  var MAX_NUMBER = 49,
      COUNT = 6,
      history = [],
      currentIndex = null,
      lottery;

  getHtmlNumber = function(number) {
    var classes = ['number'],
        range = Math.floor(number / 10) + 1;
    
    classes.push('number--range' + range.toString());
    
    return '<span class="' + classes.join(' ') + '">' + number.toString() + '</span>';
  }
  
  getHtmlNumbers = function(numbers) {
    var content = '';

    for (var i in numbers) {
      content = content + getHtmlNumber(numbers[i]);
    }
    
    return content;
  }

  renderNumbers = function() {
    var bonusElement = document.getElementById('bonus-number'),
        numbersElement = document.getElementById('numbers');

    numbersElement.innerHTML = getHtmlNumbers(history[currentIndex].numbers);
    bonusElement.innerHTML = getHtmlNumber(history[currentIndex].bonus);
  }

  showPreviousNumbers = function() {      
      if (currentIndex > 0) {
        currentIndex--;
        renderNumbers();    
      }
  }

  showNextNumbers = function() {      
      if (currentIndex === null) {
        currentIndex = 0;
      }
      else {
        currentIndex++;        
      }
      
      if (currentIndex >= history.length) {
        lottery.shuffleNumbers();
        history.push({
          numbers: lottery.getNumbers(),
          bonus: lottery.getBonusNumber()
        });
      }
      
      renderNumbers();    
  }
    
  setInitialState = function() {
    history = [];
    currentIndex = null;
  }  
    
  addEventListeners = function() {
    var btnPrevious = document.getElementById('btn-previous'),
        btnStart = document.getElementById('btn-start'),
        btnNext = document.getElementById('btn-next');
    
    btnPrevious.addEventListener("click", function() {
      showPreviousNumbers();
    });

    btnNext.addEventListener("click", function() {
      showNextNumbers();
    });

    btnStart.addEventListener("click", function() {
      setInitialState();

      showNextNumbers();
    });
  }
  
  this.start = function() {
    lottery = new Lottery(MAX_NUMBER, COUNT);
    addEventListeners();
  }
}
