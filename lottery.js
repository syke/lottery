function Lottery(maxNumber, count) {
	var generateNumbers,
		shuffle;

	generateNumbers = function() {
		for (var i = 1; i <= maxNumber; i++) {
			this.numbers.push(i);
		}
	}.bind(this);
	
	shuffle = function(a) {
		var j, x, i;

		for (i = a.length - 1; i > 0; i--) {
			j = Math.floor(Math.random() * (i + 1));
			x = a[i];
			a[i] = a[j];
			a[j] = x;
		}
	}

	sortNumbers = function(numbers) {
    return numbers.sort( function(a, b) {
      return a - b;
    });
	}

  this.shuffleNumbers = function() {
    shuffle(this.numbers);
  }
 
  this.getNumbers = function() {
    return sortNumbers(this.numbers.slice(0, this.count));
  }

  this.getBonusNumber = function() {
    return this.numbers.slice(this.count, this.count + 1);
  }

	this.maxNumber = maxNumber;
	this.count = count;
	this.numbers = [];

	generateNumbers();
}

